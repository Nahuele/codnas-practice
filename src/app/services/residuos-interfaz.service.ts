import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class ResiduosInterfazService {

  constructor(private http: HttpClient) {}

  public getResiduos() {
   return this.http
      .get('assets/interactions_1gzm_comma.csv', { responseType: 'text' })
  }


}
