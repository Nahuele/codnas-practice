import { TestBed } from '@angular/core/testing';

import { ResiduosInterfazService } from './residuos-interfaz.service';

describe('ResiduosInterfazService', () => {
  let service: ResiduosInterfazService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ResiduosInterfazService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
