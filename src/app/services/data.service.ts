import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable, BehaviorSubject} from 'rxjs';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class DataService {

  private wsUrl = environment.ws;

  constructor(private http: HttpClient) {}

  public getInfo(params: HttpParams): Observable<any> {
    // return this.http.get<DB[]>('./assets/miniDB_test.json');
    // let httpHeaders: HttpHeaders = new HttpHeaders();
    // httpHeaders = httpHeaders.append('codnasQ-version', '1');
    const paramsDue = params.toString().replace("=", "/");
    const url = this.wsUrl + paramsDue;
    return this.http.get(
      url, {
        responseType: 'json',
        // headers: httpHeaders

      }
    );
  }

  private searchSource = new BehaviorSubject<string>(null);
  currentSearch = this.searchSource.asObservable();

  //function que cambia el value de mi search
  changeSearch(campoSearch: string) {
    this.searchSource.next(campoSearch);
  }
}
