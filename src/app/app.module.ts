import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppBootstrapModule} from './bootstrap/bootstrap.module';
import { CollapseModule } from 'ngx-bootstrap/collapse';


import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HttpClientModule} from '@angular/common/http';


import {MaterialModule} from './material/material.module';
import {AdvancedsearchComponent} from './components/advancedsearch/advancedsearch.component';
import {FooterComponent} from './footer/footer.component';
import {HomeComponent} from './components/home/home.component';
import {FeatureViewerComponent} from './components/feature-viewer-component/feature-viewer-component.component';
import {AboutComponent} from './static-pages/about/about.component';
import {StatsComponent} from './static-pages/stats/stats.component';
import {HowtouseitComponent} from './static-pages/howtouseit/howtouseit.component';
import {HeadermenuComponent} from './headermenu/headermenu.component';
import {ContactComponent} from './static-pages/contact/contact.component';
import {VisualizatorComponent} from './components/visualizator/visualizator.component';
import {BrowserComponent} from './components/browser/browser.component';
import {ClusterviewComponent} from './components/clusterview/clusterview.component';
import {FormsModule} from '@angular/forms';
import {NglViewerComponent} from './components/ngl-viewer/ngl-viewer.component';
import {ResiduosInterfazService} from './services/residuos-interfaz.service';


@NgModule({
  declarations: [
    AppComponent,
    AdvancedsearchComponent,
    FooterComponent,
    HomeComponent,
    FeatureViewerComponent,
    AboutComponent,
    StatsComponent,
    HowtouseitComponent,
    AdvancedsearchComponent,
    HeadermenuComponent,
    ContactComponent,
    VisualizatorComponent,
    BrowserComponent,
    ClusterviewComponent,
    NglViewerComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MaterialModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    AppBootstrapModule,
    CollapseModule.forRoot()
  ],
  providers: [ResiduosInterfazService],
  bootstrap: [AppComponent],
})
export class AppModule {}
