import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-headermenu',
  templateUrl: './headermenu.component.html',
  styleUrls: ['./headermenu.component.css'],
})
export class HeadermenuComponent implements OnInit {
  constructor(private router: Router) {}
  isCollapsed = true;

  ngOnInit(): void {}

  goToRuta(id_route: string) {
    this.router.navigate(['visualizator', id_route]);
  }
}
