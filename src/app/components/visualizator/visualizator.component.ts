import { Component, OnInit, OnDestroy } from '@angular/core';
import LiteMol from 'litemol';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-visualizator',
  templateUrl: './visualizator.component.html',
  styleUrls: ['./visualizator.component.css'],
})
export class VisualizatorComponent implements OnInit {
  title = '1TQN';

  constructor(private route: ActivatedRoute) {}

  ngOnInit() {
    let id = this.route.snapshot.paramMap.get('id');
    var plugin = LiteMol.Plugin.create({ target: '#litemol' });
    plugin.loadMolecule({
      id: '1tqn',
      url: 'https://www.ebi.ac.uk/pdbe/static/entry/1tqn_updated.cif',
      format: 'cif', // default
    });
  }
}
