import { Component, OnInit, ViewChild, Renderer2 } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  clusterID: string;

  constructor(
    private dataService: DataService,
    private _snackBar: MatSnackBar
  ) {}

  ngOnInit() {}

  Search(event, id:string) {
    if (id.length == 4) {
      this.clusterID = id;
      this.dataService.changeSearch(id);
      // console.log('entry buscada', this.clusterID);
    } else {

	    this.dataService.changeSearch(null)
    }
    // openNotFound() {
    //   this._snackBar.open('Not founded, its a PDB??' , '', {
    //     duration: 2000,
    //   });

    //   //  this._snackBar.dismiss();
  }
}
