import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { ResiduosInterfazService } from '../../services/residuos-interfaz.service';
import * as $ from 'jquery';

declare const NGL: any;


@Component({
  selector: 'app-ngl-viewer',
  templateUrl: './ngl-viewer.component.html',
  styleUrls: ['./ngl-viewer.component.css'],
})
export class NglViewerComponent implements OnInit {
  // interfaz;
  // userArray: Interfaces[] = [];

  interfaz_residuos = [];
  pdb_query: string = '3zpg-1';
  pdb_target: string = '3zpc-1';
  //  pdb_query: string = '1gzm';
  // pdb_target: string = '1u19';

  constructor(
    private route: ActivatedRoute,
    private http: HttpClient // public interfaces: ResiduosInterfazService,
  ) {
    // this.interfaz = this.interfaces.getResiduos()
    // this.interfaz.subscribe((data) => {
    //   let csvToRowArray = data.split('\n');
    //   for (let index = 1; index < csvToRowArray.length - 1; index++) {
    //     let row = csvToRowArray[index].split(',');
    //     this.userArray.push(
    //       new Interfaces(row[0], parseInt(row[1], 10), row[2], row[3])
    //     );
    //   }
    //   // this.interfaz_residuos = this.userArray.map((a) => a.pos1);
    //   this.interfaz_residuos.push(this.userArray.map((a) => a.pos1))
    //   // console.log(this.interfaz_residuos)
    //
    // });

  }
// TODO : refactorear las funciones, cambiar las variables, y reemplazar el proxy

  public ngOnInit() {

    var myUrl = 'http://ufq.unq.edu.ar/codnasq/topmatch/quaternary/3zpg/3zpg-1_3zpc-1/';
    var proxy = 'https://cors-anywhere.herokuapp.com/';
    var finalURL1 = proxy + myUrl + this.pdb_query + '.pdb';
    var finalURL2 = proxy + myUrl + this.pdb_target + '.pdb';

    // var stage = new NGL.Stage('NGL');
    // // Handle window resizing
    // window.addEventListener(
    //   'resize',
    //   function (event) {
    //     stage.handleResize();
    //   },
    //   false
    // );
    var stagetwo = new NGL.Stage('viewport');

    window.addEventListener(
      'resize',
      function (event) {
        stagetwo.handleResize();
      },
      false
    );
    var residuos =
      '210, 279, 214, 236, 276, 194, 196, 217, 213, 221, 223, 199, 202, 216, 213, 202, 275, 205, 217, 220, 231, 201, 209, 231, 229, 259, 225, 220, 197, 220, 203, 262, 228, 217, 196, 213, 227, 273, 223, 232, 209, 201, 224, 222, 199, 224, 201, 220, 209, 204, 224, 227, 196, 227, 274, 231, 194, 206, 225, 216, 220, 208, 228, 220, 201, 217, 273, 227, 272, 198, 223, 278, 276, 213, 196, 195, 223, 234, 208, 226, 222, 220, 277, 202, 209, 205, 213, 218, 217, 221, 201, 228, 224, 279, 200, 229, 228, 228, 199, 232, 227, 277, 214, 231, 235, 219, 201, 209, 276, 231, 205, 205, 193, 223, 220, 224, 273, 207, 196, 5, 196, 202, 216, 232, 198, 220, 228, 196, 277, 228, 194, 279, 206, 231, 205, 201, 226, 276, 223, 201, 220, 205, 225, 223, 194, 231, 196, 225, 276, 228, 232, 221, 276, 217, 231, 205, 221, 220, 201, 230, 217, 225, 209, 227, 195, 194, 228, 228, 213, 273, 205, 201, 193, 196, 223, 203, 220, 216, 228, 224, 232, 229, 227, 208, 225, 232, 259, 220, 209, 205, 201, 221, 220, 228, 195, 194, 197, 223, 205, 213, 197, 218, 205, 214, 233, 277, 196, 279, 200, 278, 227, 202, 196';

    // stage.loadFile(`./assets/${this.pdb_query}.pdb`).then(function (b) {
    //   b.addRepresentation('surface', {
    //     color: 'lightgreen',
    //     smoothSheet: true,
    //     opacity: 0.3,
    //     depthWrite: false,
    //     side: 'front',
    //     quality: 'high',
    //   });
    //   b.addRepresentation('surface', { sele: residuos, color: 'red' }); // el sele solo lee en ('resNum1, resNum2, resNum3...')
    //   b.autoView();
    //   return b;
    // });


    // Promise.all([
    //   stagetwo.loadFile(finalURL1).then(function (o) {
    //     o.addRepresentation('cartoon', {color: 'lightgreen'});
    //     o.autoView();
    //     return o;
    //   }),
    //   stagetwo.loadFile(finalURL2).then(function (o) {
    //     o.addRepresentation('cartoon', {color: 'tomato'});
    //     o.autoView();
    //     return o;
    //   }),
    // ]);


    // alternative options
    // button
    // document.getElementById("verinterfaz").addEventListener('click', showInterfaces);
    // var pdb_query = this.pdb_query;
    // var clickedInterface = false;
    //
    //
    // function showInterfaces() {
    //   // var x = document.getElementById( "verinterfaz" );
    //   clickedInterface = !clickedInterface;
    //
    //   if (clickedInterface) {
    //
    //     stagetwo.removeAllComponents();
    //     stagetwo.loadFile(`./assets/${pdb_query}.pdb`).then(function (b) {
    //       b.addRepresentation('surface', {
    //         color:       'lightgreen',
    //         smoothSheet: true,
    //         opacity:     0.3,
    //         depthWrite:  false,
    //         side:        'front',
    //         quality:     'high',
    //         useWorker:   false,
    //       });
    //       b.addRepresentation('surface', {
    //         sele: residuos, color: 'red', useWorker: false
    //       }); // el sele solo lee en ('resNum1, resNum2, resNum3...')
    //       b.autoView();
    //       return b;
    //     });
    //   } else {
    //     stagetwo.removeAllComponents();
    //
    //     Promise.all([
    //       stagetwo.loadFile(finalURL1).then(function (o) {
    //         o.addRepresentation('cartoon', {color: 'lightgreen'});
    //         o.autoView();
    //         return o;
    //       }),
    //       stagetwo.loadFile(finalURL2).then(function (o) {
    //         o.addRepresentation('cartoon', {color: 'tomato'});
    //         o.autoView();
    //         return o;
    //       }),
    //     ]);
    //
    //   }}

    stagetwo.loadFile(`./assets/${this.pdb_query}.pdb`).then(function (b) {
      b.setPosition([100, 0, 0])

      b.addRepresentation('surface', {
        color:       'lightgreen',
        smoothSheet: true,
        opacity:     0.3,
        depthWrite:  false,
        side:        'front',
        quality:     'high',
        useWorker:   false,
      });
      b.addRepresentation('surface', {
        sele: residuos, color: 'red', useWorker: false
      }); // el sele solo lee en ('resNum1, resNum2, resNum3...')
      var pa = b.structure.getPrincipalAxes();
      stagetwo.animationControls.rotate(pa.getRotationQuaternion(), 1500);
      stagetwo.autoView();
     // return b;
    });
    Promise.all([
      stagetwo.loadFile(finalURL1).then(function (o) {

        // o.setPosition([20, 0, 0])
        o.addRepresentation('cartoon', {color: 'lightgreen'});
        o.autoView();
       // return o;
      }),
      stagetwo.loadFile(finalURL2).then(function (o) {

        o.addRepresentation('cartoon', {color: 'tomato'});
        o.autoView();
       // return o;

      }),
    ]);
  }

}
