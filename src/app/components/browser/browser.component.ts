import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import {DataService} from 'src/app/services/data.service';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'app-browser',
  templateUrl: './browser.component.html',
  styleUrls: ['./browser.component.css'],
})
export class BrowserComponent implements OnInit {
  url = environment.ws + 'clusters/';
  items = [];
  clusterID: string;

  constructor(
    private http: HttpClient,
    private route: ActivatedRoute,
    private router: Router,
    private dataService: DataService
  ) {
    this.http
      .get(this.url)
      .toPromise()
      .then((data) => {
        // console.log(data);

        for (let key in data) {
          if (data.hasOwnProperty(key)) {
            this.items.push(data[key]);
          }
        }
      });
  }
  ngOnInit(): void {

  }

  mostrarEjemplo(event, id) {
    this.clusterID = id;
    this.dataService.changeSearch(id);
    console.log(this.clusterID);
  }
}
