import { Component, OnInit } from '@angular/core';
import { FeatureViewer } from 'feature-viewer-typescript/lib';

@Component({
  selector: 'app-feature-viewer-component',
  templateUrl: './feature-viewer-component.component.html',
  styleUrls: ['./feature-viewer-component.component.css'],
})
export class FeatureViewerComponent implements OnInit {
  constructor() {}

  ngOnInit() {
    const proteinsequence =
      'MTKFTILLISLLFCIAHTCSASKWQHQQDSCRKQLQGVNLTPCEKHIMEKIQGRGDDDDDDDDDNHILRTMRGRINYIRRNEGKDEDEE';
    const fv = new FeatureViewer(proteinsequence, '#myfv', {
      showAxis: true,
      showSequence: true,
      toolbar: false,
      toolbarPosition: 'left',
      zoomMax: 10,
      flagColor: '#EE6656',
      sideBar: true,
      animation: true,
      backgroundcolor: '#E6F8DC',
      brushActive: true,
    });

    fv.addFeatures([
      {
        // simple rect
        type: 'rect',
        id: 'useUniqueId',
        data: [
          {
            x: 50,
            y: 78,
            tooltip: '<button class="myButton">Button</button>',
          },
        ],
      },
      {
        // circles
        type: 'circle',
        id: 'mycircle',
        label: 'Circle feature',
        data: [
          { x: 10, y: 100 },
          { x: 50, y: 70 },
          { x: 40, y: 60, color: '#00ac8f', tooltip: 'I have different color' },
        ],
        color: '#61795e',
      },
      {
        // curve (height and yLim) with tooltip and subfeatures
        type: 'curve',
        id: 'mycurve',
        label: 'Curve label',
        data: [
          { x: 1, y: 0 },
          { x: 40, y: 102 },
          { x: 80, y: 5 },
          { x: 50, y: 184 },
          { x: 75, y: 4 },
        ],
        height: 1,
        yLim: 200,
        color: '#00babd',
        tooltip:
          '<b>Very</b> <span style="color: #C21F39">Stylable</span> <b><i><span style="color: #ffc520">Tooltip </span></i></b>',
        subfeatures: [
          {
            type: 'rect',
            data: [
              { x: 20, y: 30 },
              { x: 15, y: 45 },
              { x: 70, y: 76, label: 'myRect', tooltip: 'myTooltip' },
            ],
            id: 'aDifferentId',
            label: 'I am a subfeature!',
          },
        ],
      },
    ]);
  }
}
