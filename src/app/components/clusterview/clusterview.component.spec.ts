import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClusterviewComponent } from './clusterview.component';

describe('ClusterviewComponent', () => {
  let component: ClusterviewComponent;
  let fixture: ComponentFixture<ClusterviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClusterviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClusterviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
