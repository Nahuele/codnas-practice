import {
  Component,
  OnInit,
  Input,
  Output,
  OnChanges,
  ViewChild,
  ChangeDetectorRef,
} from '@angular/core';
import {DataService} from 'src/app/services/data.service';
import {HomeComponent} from '../home/home.component';
import {Subscription} from 'rxjs';
import {ActivatedRoute, Router} from '@angular/router';
import {HttpParams} from '@angular/common/http';

declare const NGL: any;


@Component({
  selector:    'app-clusterview',
  templateUrl: './clusterview.component.html',
  styleUrls:   ['./clusterview.component.css'],
})
export class ClusterviewComponent implements OnInit {
  // se puede customizar el @Input con get and set, por ahora lo dejo asi xq son strings simples
  //  @Input('clusterID') clusterID: string;
  campoSearch: string;

  constructor(private dataService: DataService, private route: ActivatedRoute) {}

  datos;
  private testingSub: Subscription;
  pdb_query: string;

  ngOnInit(): void {
    let params = new HttpParams();
    let codigo = this.route.snapshot.paramMap.get('Cluster_ID');
    params = params.set('clusters', codigo);
    console.log('link activado', codigo);


    if (codigo && codigo.length == 4) {
      this.dataService.getInfo(params).subscribe((data) => {
        this.campoSearch = data[0];
        if (this.campoSearch.length > 0) {
          console.log('busqueda exitosa');
          console.log(this.campoSearch);
        }
      });
      this.threeDview(codigo);

    } else {
      console.log('no se pudo leer la url');
      //  this.dataService.getInfo(params).subscribe((data) => {
      //     let campoSearch = data.find((obj) => obj.Cluster_ID === this.campoSearch);
      //     // console.log('testingsub', this.testingSub);
      //     this.datos = campoSearch;
      //     try {
      //       this.pdb_query = campoSearch.Cluster_ID;
      //       this.threeDview(this.pdb_query);
      //       // this.getRoute(this.datos)
      //
      //     } catch (e) {
      //       console.log(e, 'error aqui');
      //       this.datos = null;
      //       this.campoSearch = null;
      //     }
      //     console.log('pdb_query desde init', this.pdb_query);
      //   });
    }


  };


  threeDview(queryPdb:string) {
    queryPdb = queryPdb.toLocaleUpperCase();

    // NGL visualizator
    // NGL.DatasourceRegistry.add(
    //   "data", new NGL.StaticDatasource("//files.rcsb.org/view/")
    // );
// Create NGL Stage object
    var stage = new NGL.Stage("viewport");
// Handle window resizing
    window.addEventListener("resize", function (event) {
      stage.handleResize();
    }, false);
// Code for example: color/bfactor
    stage.loadFile(`rcsb://${queryPdb}.pdb`).then(function (o) {
      o.addRepresentation("cartoon", {color: "bfactor"});
      o.autoView();
    });
  }

  // con esta funcion cambio el query de busqueda
  // newQuery(pdb: string) {
  //   this.dataService.changeSearch(pdb);
  // }

  ngOnDestroy() {
    this.testingSub.unsubscribe();
  }

  // getRoute(cluster) {
  //   // console.log(cluster)
  //   this.route.navigate(['/cluster', cluster.Cluster_ID])
  // }
}
