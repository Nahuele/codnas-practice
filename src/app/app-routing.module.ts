import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {HomeComponent} from './components/home/home.component';
import {AboutComponent} from './static-pages/about/about.component';
import {StatsComponent} from './static-pages/stats/stats.component';
import {HowtouseitComponent} from './static-pages/howtouseit/howtouseit.component';
import {AdvancedsearchComponent} from './components/advancedsearch/advancedsearch.component';
import {ContactComponent} from './static-pages/contact/contact.component';
import {FeatureViewerComponent} from './components/feature-viewer-component/feature-viewer-component.component';
import {VisualizatorComponent} from './components/visualizator/visualizator.component';
import {BrowserComponent} from './components/browser/browser.component';
import {ClusterviewComponent} from './components/clusterview/clusterview.component';
import {NglViewerComponent} from './components/ngl-viewer/ngl-viewer.component';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'about', component: AboutComponent},
  {path: 'stats', component: StatsComponent},
  {path: 'howto', component: HowtouseitComponent},
  {path: 'browser', component: BrowserComponent},
  {path: 'cluster', component: ClusterviewComponent},
  {path: 'cluster/:Cluster_ID', component: ClusterviewComponent},
  {path: 'advancedsearch', component: AdvancedsearchComponent},
  {path: 'contact', component: ContactComponent},
  {path: 'fvtest', component: FeatureViewerComponent},
  {
    path:     'visualizator',
    children: [
      {path: 'NGL', component: NglViewerComponent},
      {path: 'litemol', component: VisualizatorComponent},
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
