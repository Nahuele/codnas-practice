import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

declare const NGL: any;

@Component({
  selector: 'app-visualizator',
  templateUrl: './visualizator.component.html',
  styleUrls: ['./visualizator.component.css'],
})
export class VisualizatorComponent implements OnInit {
  selectedPdb: string;

  pdbid: string;
  chainid: string;
  structure_title: string;
  colormap: any;

  sv1: any;
  struct: any;

  constructor(
    public dialogRef: MatDialogRef<VisualizatorComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.selectedPdb = data.selectedPdb;
    [this.pdbid, this.chainid] = this.selectedPdb.split('.');

    this.computeColormap(data.track, data.mapping['mapping']);

    this.structure_title = data.mapping['title'];
  }

  ngOnInit() {
    // populate structures
    this.sv1 = new NGL.Stage('ngl-viewport', { backgroundColor: 'white' });

    let tooltip = document.createElement('div');
    Object.assign(tooltip.style, {
      display: 'none',
      position: 'absolute',
      zIndex: 10,
      pointerEvents: 'none',
      backgroundColor: 'rgba(0, 0, 0, 0.6)',
      color: 'lightgrey',
      padding: '0.5em',
      fontFamily: 'Roboto',
    });
    this.sv1.viewer.container.appendChild(tooltip);

    this.get_pdb(this.pdbid);

    this.sv1.signals.hovered.add(function (pickingProxy) {
      if (pickingProxy && (pickingProxy.atom || pickingProxy.bond)) {
        let atom = pickingProxy.atom || pickingProxy.closestBondAtom;
        let cp = pickingProxy.canvasPosition;
        tooltip.innerText = 'ATOM: ' + atom.qualifiedName();
        tooltip.style.bottom = cp.y + 3 + 'px';
        tooltip.style.left = cp.x;
        tooltip.style.display = 'block';
      } else {
        tooltip.style.display = 'none';
      }
    });

    // this.sv1.signals.clicked.add(function (pickingProxy) {...});
  }

  ngOnDestroy() {
    this.sv1.removeAllComponents();
    this.sv1 = null;
  }

  get_pdb(pdb) {
    if (this.struct) {
      this.sv1.removeAllComponents();
    }
    this.sv1
      .loadFile('rcsb://' + pdb.toLowerCase())
      .then(this.defaultStructureRepresentation);
  }

  takePicture() {
    this.sv1
      .makeImage({
        trim: true,
        factor: 3,
        antialias: true,
        transparent: true,
      })
      .then((blob) => {
        let url = URL.createObjectURL(blob);
        let downloadLink = document.createElement('a');
        downloadLink.href = url;
        downloadLink.download = `mdb3-${this.pdbid}.png`;
        document.body.appendChild(downloadLink);
        downloadLink.click();
        document.body.removeChild(downloadLink);
      });
  }

  private defaultStructureRepresentation = (struct) => {
    if (struct.type !== 'structure') return;

    this.struct = struct;

    this.struct.addRepresentation('cartoon', {
      assembly: 'AU',
      sele: `:${this.chainid}`,
      color: this.colormap,
    });

    this.struct.addRepresentation('cartoon', {
      assembly: 'AU',
      sele: `all and not :${this.chainid}`,
      color: 'bisque',
    });

    this.struct.addRepresentation('ball+stick', {
      sele: 'hetero and not water',
    });

    this.struct.addRepresentation('cartoon', {
      sele: 'dna or rna',
      color: 'gray',
    });

    this.struct.addRepresentation('base', {
      sele: 'dna or rna',
      color: 'gray',
    });

    let pa = this.struct.structure.getPrincipalAxes();
    this.sv1.animationControls.rotate(pa.getRotationQuaternion(), 1500);

    this.sv1.autoView();
  };

  private computeColormap(track, mapping) {
    let rev_mapping = {};
    for (let m of mapping) {
      if (m['chain_id'] === this.chainid) {
        for (let key in m['mapping']) {
          if (m['mapping'].hasOwnProperty(key)) {
            let pdbidx = m['mapping'][key];
            rev_mapping[pdbidx] = +key;
          }
        }
        break;
      }
    }

    let d = {};
    track.forEach((region) => {
      let start = rev_mapping[region.x - 1];
      let end = rev_mapping[region.y - 1];
      let color = region.color;

      if (start && end) {
        if (!d.hasOwnProperty(color)) d[color] = [];
        d[color].push(`${start}-${end}`);
      }
    });

    let selectionScheme = [];
    for (let color in d) {
      selectionScheme.push([color, d[color].join(' or ')]);
    }
    selectionScheme.push(['lightgray', '*']);

    this.colormap = NGL.ColormakerRegistry.addSelectionScheme(
      selectionScheme,
      this.selectedPdb
    );
  }
}
