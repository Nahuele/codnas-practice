// archivo inutil de interfaces, NO TENER EN CUENTA
interface DB {
  Cluster_ID: string;
  Oligomeric_State: number;
  PDB_ID_query: string;
  Biological_Assembly_query: number;
  PDB_ID_target: string;
  Biological_Assembly_target: number;
  Type_of_alignment: string;
  Rank_of_alignment: number;
  Structural_similarity: number;
  Query_cover: number;
  Target_cover: number;
  Structurally_equivalent_residue_pairs: number;
  Query_cover_based_on_alignment_length: number;
  Target_cover_based_on_alignment_length: number;
  Typical_distance_error: number;
  RMSD: number;
  Sequenceidentity: number;
  Permutations: number;
  Query_resolution: number;
  Target_resolution: number;
  Query_method: string;
  Target_method: string;
  Query_length: number;
  Target_length: number;
  Query_name: string;
  Target_name: string;
  Query_organism: string;
  Target_organism: string;
  Query_ligands: string;
  Target_ligands: string;
  Query_description: string;
  Target_description: string;
  Target_UniProt_ID: string;
  Target_Gene_names: string;
  Target_Pfam: string;
  Query_UniProt_ID: string;
  Query_Gene_names: string;
  Query_Pfam: string;
  grupo: string;
  target_pH: string;
  target_Temperature: number;
  query_pH: number;
  query_Temperature: number;
  QueryChainID: string;
  TargetChainID: string;
  maxRMSD: number;
}

class Interfaces {
  res1: string;
  pos1: number;
  ato1: string;
  chain1: string;

  constructor(res1: string,
              pos1: number,
              ato1: string,
              chain1: string) {
    this.res1 = res1;
    this.pos1 = pos1;
    this.ato1 = ato1;
    this.chain1 = chain1;
  }
}
