import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HowtouseitComponent } from './howtouseit.component';

describe('HowtouseitComponent', () => {
  let component: HowtouseitComponent;
  let fixture: ComponentFixture<HowtouseitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HowtouseitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HowtouseitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
