# CodnasTest

El proyecto este fue generado para:


* practicar javascript, angular, css, html, git, y heroku
* destruirlo y hecharlo a perder todo lo que queramos

Se puede ver desde Heroku  [https://codnas-test.herokuapp.com/] (tarda un poco en cargar, y está linkeada a otro repositorio de GitHub, así que cualquier cambio que le hagan no se va a poder ver online a menos que yo lo actualice a mano).

Para correr local hay que instalar npm

luego desde la carpeta del repo en consola correr "ng run start-dev"


This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.0.

## Ultima actualizacion


- Se enlazó el back-end API https://gitlab.com/Nahuele/api-server-test con este front-end.
- Ahora hay dos DB incluidas en las carpetas "assets". Una DB de maximos de ejemplo, y otra de los totales, esta úlltima se accede desde el menú Browser
- Se puede acceder a un cluster desde la URL. 
- Se lee la URL, si existe en nuestra DB (consultando la API) se va a mostrar el contenido en la vista del cluster ../cluster/codigoPDB. En caso contrario aparece una leyenda en la página, y también en la consola, así que tenganla abierta.
